Attribute VB_Name = "mPlayer"

Option Explicit
'Public variables for playback
'Const for channels
Private Const conNumChan As Long = 64
Private lngNumChan As Long
Private StreamHandle(conNumChan) As Long, ChannelHandle(conNumChan) As Long
Public lCurrentChannel As Long
Public bSlider As Boolean             '// arrastrando slider posbar
Private Enum pePlayer
    NotLoaded = 0
    Stopped = 1
    Playing = 2
    Paused = 3
End Enum

Public Type typeHttpSongInfo
    Title As String
    StationName As String
    Artist As String
    URL As String
    birate As Long
    Genre As String
End Type

Public tCurrent_HttpSongInfo As typeHttpSongInfo
Private ePlayerState(conNumChan) As pePlayer

'Private variables for the spectrum
Private blnSpectrumOn As Boolean

'Private variables for FX
Private lngEQ(9) As Long, lngFX(8) As Long

'public variables for playback
Public strCurrentFile As String
Dim iMetaNum As Integer

''For various functions for sringfrompointer
Private Declare Function lstrlenA Lib "kernel32.dll" (ByVal lpString As String) As Long
Private Declare Function lstrlen Lib "kernel32.dll" (ByVal lpString As String) As Long

Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Private Declare Function ConvCStringToVBString Lib "kernel32" Alias "lstrcpyA" (ByVal lpsz As String, ByVal pt As Long) As Long

'\\ Pointer validation in StringFromPointer
Declare Function IsBadStringPtrByLong Lib "kernel32" Alias "IsBadStringPtrA" (ByVal lpsz As Long, ByVal ucchMax As Long) As Long
Private Declare Function SysAllocStringByteLen Lib "oleaut32" (ByVal Ptr As Long, ByVal Length As Long) As String


Public Function GetICYtag(StreamHandle As Long) As Boolean
Dim numTags As Long
Dim namepointer As Long, valuepointer As Long, Length As Long, typetag As Long
Dim Value As String, Name As String
   Call FSOUND_Stream_GetNumTagFields(StreamHandle, numTags)
Dim i
For i = 0 To numTags - 1
Call FSOUND_Stream_GetTagField(StreamHandle, i, typetag, namepointer, valuepointer, Length)
Value = GetStringFromPointer(valuepointer)
Name = GetStringFromPointer(namepointer)
Debug.Print Name & ": " & Value
Next i


End Function


'FMOD Functions
Public Sub FMOD_Initialize(Optional BufferSize As Long = 50, Optional MixerRate As Long = 44100, Optional MaxChannels As Long = 16, Optional Flags As FSOUND_INITMODES = 4, Optional Driver As FSOUND_OUTPUTTYPES = 2, Optional MixerType As FSOUND_MIXERTYPES = 4, Optional Device As Long = 0)
    On Error GoTo hell

    'Pre-initialize
    'These must be called before the initit
    FSOUND_SetDriver (Device)
    FSOUND_SetOutput (Driver)
    FSOUND_SetMixer (MixerType)

    FSOUND_SetBufferSize (100)

    If (MaxChannels > conNumChan) Then
        MsgBox "You can only have a maximum of " & conNumChan & " channels.", vbCritical + vbOKOnly, "Error"
        Exit Sub
    End If

    lngNumChan = MaxChannels
    'Initiate fmod
    If (FSOUND_Init(MixerRate, MaxChannels, Flags) = 0) Then    '1 or 2 or 4
        MsgBox "FMOD Cannot initialize because of an error." & _
               vbCrLf & (FSOUND_GetErrorString(FSOUND_GetError)), vbCritical + vbApplicationModal + vbOKOnly
    End If

    FSOUND_Stream_SetBufferSize (BufferSize)

    Exit Sub
hell:
    MsgBox "MAHESH MP3 Cannot initialize because of an error." & vbCrLf & err.Description, vbCritical + vbApplicationModal + vbOKOnly
End Sub


Public Function FMOD_GetCPU() As Single
    On Error Resume Next
    FMOD_GetCPU = FSOUND_GetCPUUsage
End Function

Public Function FMOD_GetNumChannels() As Long
    On Error Resume Next
    FMOD_GetNumChannels = lngNumChan
End Function

Public Function FMOD_GetMaxChannels() As Long
    On Error Resume Next
    FMOD_GetMaxChannels = conNumChan
End Function

Public Function FMOD_GetStatus(Channel As Long) As Boolean
    On Error Resume Next
    'Detect end of file
    If (Stream_GetPosition(Channel) = Stream_GetDuration(Channel)) Then
        FMOD_GetStatus = False
    Else
        FMOD_GetStatus = True
    End If

End Function

Public Function FMOD_Version() As Single
    On Error Resume Next
    FMOD_Version = FSOUND_GetVersion
End Function

Public Sub FMOD_Terminate()
    On Error Resume Next
    'Force FMOD to close
    FSOUND_Close
End Sub

Public Function Stream_GetState(Channel As Long) As Long
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Function

    Stream_GetState = ePlayerState(Channel)
End Function

Public Sub Stream_SetFrequency(Channel As Long, Value As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    FSOUND_SetFrequency Channel, Value
End Sub

Public Sub Stream_SetMute(Channel As Long, Value As Boolean)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    FSOUND_SetMute Channel, Value
End Sub

Public Sub Stream_Open(File As String, Optional Mode As FSOUND_MODES = 16, Optional Channel As Long = 0, Optional Play As Boolean = False, Optional Volume As Long = 255)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    strCurrentFile = File

    'Stop the current song
    Stream_Stop Channel

    'Open the new stream
    StreamHandle(Channel) = FSOUND_Stream_Open(File, Mode, 0, 0)

    'Change the playstate
    'Test to make sure the file is loaded
    If (StreamHandle(Channel) <> 0) Then
        ePlayerState(Channel) = Stopped
    End If

    If Play = True Then
        Stream_Play Channel
        Stream_SetVolume Channel, Volume
    End If
End Sub

Public Sub Stream_Play(Channel As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    If (FSOUND_GetPaused(StreamHandle(Channel)) = 1) Then Stream_Pause Channel
    'Play the stream in a channel
    ChannelHandle(Channel) = FSOUND_Stream_Play(Channel, StreamHandle(Channel))

    'Change the playstate
    If (ChannelHandle(Channel) <> 0) Then
        ePlayerState(Channel) = Playing
    End If
End Sub

Public Sub Stream_PlayEx(Channel As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    If (FSOUND_GetPaused(StreamHandle(Channel)) = 1) Then Stream_Pause Channel
    'Play the stream in a channel
   ' ChannelHandle(Channel) = FSOUND_Stream_PlayEx(Channel, StreamHandle(Channel))

    'Change the playstate
    If (ChannelHandle(Channel) <> 0) Then
        ePlayerState(Channel) = Playing
    End If
End Sub
Public Sub Stream_Pause(Channel As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    'Pause the stream
    If (FSOUND_GetPaused(ChannelHandle(Channel)) = False) Then
        FSOUND_SetPaused ChannelHandle(Channel), True

        'Stream is paused
        If (StreamHandle(Channel) <> 0) Then
            ePlayerState(Channel) = Paused
        End If
    Else

        FSOUND_SetPaused ChannelHandle(Channel), False

        'Stream is playing
        If (StreamHandle(Channel) <> 0) Then
            ePlayerState(Channel) = Playing
        End If
    End If


End Sub

Public Sub Stream_Stop(Channel As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub

    If (StreamHandle(Channel) <> 0) Then

        'stop all sounds
        FSOUND_Stream_Stop StreamHandle(Channel)

        StreamHandle(Channel) = 0

        ePlayerState(Channel) = Stopped

    End If

End Sub

Public Sub Stream_SetVolume(Channel As Long, Value As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub
    '// volumen 0 -> 255
    FSOUND_SetVolume ChannelHandle(Channel), Value

End Sub

Public Function Stream_GetVolume(Channel As Long) As Long
    On Error Resume Next
    If (Channel > lngNumChan) Then Exit Function
    Stream_GetVolume = FSOUND_GetVolume(Channel)
End Function

Public Sub Stream_SetBalance(Channel As Long, Value As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub
    'Set balance
    FSOUND_SetPan ChannelHandle(Channel), Value

End Sub

Public Function Stream_GetPosition(Channel As Long) As Long
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Function
    'Current position
    Stream_GetPosition = FSOUND_Stream_GetTime(StreamHandle(Channel)) / 1000

End Function

Public Sub Stream_SetPosition(Channel As Long, PosValue As Long)
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Sub
    'Current position
    FSOUND_Stream_SetTime (StreamHandle(Channel)), PosValue * 1000

End Sub

Public Function Stream_GetDuration(Channel As Long) As Long
    On Error Resume Next
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Function
    'Length
    Stream_GetDuration = FSOUND_Stream_GetLengthMs(StreamHandle(Channel)) / 1000

End Function


Public Sub FX_SetChorus(sngWetDryMix As Single, sngDepth As Single, sngFeedback As Single, sngFrequency As Single, lngWaveform As Long, sngDelay As Single, lngPhase As Long)
    On Error Resume Next
    Call FSOUND_FX_SetChorus(lngFX(0), sngWetDryMix, sngDepth, sngFeedback, sngFrequency, lngWaveform, sngDelay, lngPhase)
End Sub

Public Sub FX_SetCompressor(sngGain As Single, sngAttack As Single, sngRelease As Single, sngThreshold As Single, sngRatio As Single, sngPreDelay As Single)
    On Error Resume Next
    Call FSOUND_FX_SetCompressor(lngFX(1), sngGain, sngAttack, sngRelease, sngThreshold, sngRatio, sngPreDelay)
End Sub

Public Sub FX_SetDistortion(sngGain As Single, sngEdge As Single, sngPostEQCenterFrequency As Single, sngPostEQBandwidth As Single, sngPreLowpassCuttoff As Single)
    On Error Resume Next
    Call FSOUND_FX_SetDistortion(lngFX(2), sngGain, sngEdge, sngPostEQCenterFrequency, sngPostEQBandwidth, sngPreLowpassCuttoff)
End Sub

Public Sub FX_SetEcho(sngWetDryMix As Single, sngFeedback As Single, sngLeftDelay As Single, sngRightDelay As Single, sngPanDelay As Long)
    On Error Resume Next
    Call FSOUND_FX_SetEcho(lngFX(3), sngWetDryMix, sngFeedback, sngLeftDelay, sngRightDelay, sngPanDelay)
End Sub

Public Sub FX_SetFlanger(sngWetDryMix As Single, sngDepth As Single, sngFeedback As Single, sngFrequency As Single, lngWaveform As Long, sngDelay As Single, lngPhase As Long)
    On Error Resume Next
    Call FSOUND_FX_SetFlanger(lngFX(4), sngWetDryMix, sngDepth, sngFeedback, sngFrequency, lngWaveform, sngDelay, lngPhase)
End Sub

Public Sub FX_SetGargle(lngRateHz As Long, lngWaveShape As Long)
    On Error Resume Next
    Call FSOUND_FX_SetGargle(lngFX(5), lngRateHz, lngWaveShape)
End Sub

Public Sub FX_SetI3DL2Reverb(lngRoom As Long, lngRoomHF As Long, sngRoomRolloffFactor As Single, sngDecayTime As Single, sngDecayHFRatio As Single, lngReflections As Long, sngReflectionsDecay As Single, lngReverb As Long, sngReverbDelay As Single, sngDiffusion As Single, sngDensity As Single, sngHFReference As Single)
    On Error Resume Next
    Call FSOUND_FX_SetI3DL2Reverb(lngFX(6), lngRoom, lngRoomHF, sngRoomRolloffFactor, sngDecayTime, sngDecayHFRatio, lngReflections, sngReflectionsDecay, lngReverb, sngReverbDelay, sngDiffusion, sngDensity, sngHFReference)
End Sub

Public Sub FX_SetEQ(lngIndex As Long, sngValue As Single)
    On Error Resume Next

    'Define variables
    Dim sngGain As Single, sngCenter As Single

    Select Case lngIndex
    Case 0: sngCenter = 80
    Case 1: sngCenter = 170
    Case 2: sngCenter = 310
    Case 3: sngCenter = 600
    Case 4: sngCenter = 1000
    Case 5: sngCenter = 3000
    Case 6: sngCenter = 6000
    Case 7: sngCenter = 12000
    Case 8: sngCenter = 14000
    Case 9: sngCenter = 16000
    End Select
    'Get gain
    sngGain = sngValue
    'Set EQ
    Call FSOUND_FX_SetParamEQ(lngEQ(lngIndex), sngCenter, 18, sngGain)
    '    Call FSOUND_FX_SetParamEQ(lngFX(7), sngCenter, 18, sngGain)

End Sub

Public Sub FX_SetWavesReverb(sngInGain As Single, sngReverbMix As Single, sngReverbTime As Single, sngHighFrequencyRTRatio As Single)
    On Error Resume Next
    Call FSOUND_FX_SetWavesReverb(lngFX(8), sngInGain, sngReverbMix, sngReverbTime, sngHighFrequencyRTRatio)
End Sub

Public Sub FX_Enable(fX As FSOUND_FX_MODES, Optional Channel As Long = -1000)
    On Error Resume Next
    Dim intX As Integer

    If (Channel > lngNumChan) Then Exit Sub

    'The channel must be paused before we can change it
    FSOUND_SetPaused Channel, True

    'Set the Eq FX
    If (fX = FSOUND_FX_PARAMEQ) Then
        For intX = 0 To 9
            lngEQ(intX) = FSOUND_FX_Enable(Channel, FSOUND_FX_PARAMEQ)
        Next intX
    Else
        'Set up the FX
        lngFX(fX) = FSOUND_FX_Enable(Channel, fX)
    End If


    'Unpause
    FSOUND_SetPaused Channel, False
End Sub

Public Sub FX_Disable(Optional Channel As Long = -1000)
    On Error Resume Next
    If (Channel > lngNumChan) Then Exit Sub

    'The channel must be paused before we can change it
    FSOUND_SetPaused Channel, True
    FSOUND_FX_Disable Channel
    FSOUND_SetPaused Channel, False

End Sub

Public Function Spectrum_GetLeft(Channel As Long) As Single
    On Error Resume Next
    Dim sngVolLeft As Single, sngVolRight As Single

    'Check for channel bounds
    If (Channel < 0) Or (Channel > lngNumChan) Then Exit Function
    FSOUND_GetCurrentLevels Channel, sngVolLeft, sngVolRight

    Spectrum_GetLeft = sngVolLeft
End Function

Public Function Spectrum_GetRight(Channel As Long) As Single
    On Error Resume Next
    Dim sngVolLeft As Single, sngVolRight As Single

    'Check for channel bounds
    If (Channel < 0) Or (Channel > lngNumChan) Then Exit Function
    FSOUND_GetCurrentLevels Channel, sngVolLeft, sngVolRight

    Spectrum_GetRight = sngVolRight
End Function

Public Sub Spectrum_Enable(Value As Boolean)
    On Error Resume Next
    'Spectrum
    FSOUND_DSP_SetActive FSOUND_DSP_GetFFTUnit, Value

    'Reset the display
    blnSpectrumOn = Value
End Sub

Public Function Spectrum_GetData(Channel As Long, sngData() As Single)
    On Error Resume Next
    'Retreive spectrum data on if playing
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Function

    'If blnSpectrumOn = False Then Exit Function

    If (ChannelHandle(Channel) <> 0) Then
        GetSpectrum sngData
    End If

End Function


'Driver functions
Public Function Driver_GetNum() As Long
    Driver_GetNum = FSOUND_GetNumDrivers
End Function

Public Function Driver_GetName(intDriverNum As Integer) As String
    Driver_GetName = GetStringFromPointer(FSOUND_GetDriverName(intDriverNum))
End Function

Public Sub FSOUND_METADATACALLBACK(ByVal lpName As Long, ByVal Value As Long, ByVal userdata As Long)
   Debug.Print "------" & vbCrLf & "name:(" & lpName & ") " & GetStringFromPointer(lpName) & vbCrLf & "value:(" & Value & ") " & GetStringFromPointer(Value) & vbCrLf & vbCrLf
 Select Case UCase(GetStringFromPointer(lpName))
    Case "TITLE"
        tCurrent_HttpSongInfo.Title = GetStringFromPointer(Value)
        sTextScroll = tCurrent_HttpSongInfo.Title & ":" & tCurrent_HttpSongInfo.Artist
        frmMain.Show_Message sTextScroll, True
    Case "ARTIST"
        tCurrent_HttpSongInfo.Artist = GetStringFromPointer(Value)
        'sTextScroll = tCurrent_HttpSongInfo.Title & ":" & tCurrent_HttpSongInfo.Artist
    End Select

End Sub

'---------------------------------------------------------------------------------------
' Procedure : HTTPstream_Open
' Author    : Mahesh Kurmi
' Date      : 3/16/2011
' Purpose   :
' HOW       :
' Returns   :
' Parameters:
'---------------------------------------------------------------------------------------
'
Public Function HTTPstream_Open(strUrl As String, Optional Mode As FSOUND_MODES = 16, Optional Channel As Long = 0, Optional Play As Boolean = False, Optional Volume As Long = 255) As Boolean

    Dim bResult As Boolean
    iMetaNum = 0
    On Error GoTo HTTPstream_Open_Error
    'Check for channel bounds
    If (Channel > lngNumChan) Then Exit Function
    'Stop the current song
    Stream_Stop Channel

    '  Internet streams can work with a much smaller stream buffer than normal streams because they
    '  use another level of buffering on top of the stream buffer.
    Call FSOUND_Stream_SetBufferSize(100)
    '  Here 's where we set the size of the network buffer and some buffering parameters.
    '  In this case we want a network buffer of 64k, we want it to prebuffer 60% of that when we first
    '  connect, and we want it to rebuffer 80% of that whenever we encounter a buffer underrun.
    Call FSOUND_Stream_Net_SetBufferProperties(64000, 60, 80)
    'Open the new stream
    StreamHandle(Channel) = FSOUND_Stream_Open(strUrl, FSOUND_NORMAL Or FSOUND_NONBLOCKING, 0, 0)
    'Change the playstate
    'If (StreamHandle(Channel) <> 0) Then
     Call FSOUND_Stream_Net_SetMetadataCallback(StreamHandle(Channel), AddressOf FSOUND_METADATACALLBACK, 0)
    'End If
    'If (StreamHandle(Channel)) Then
    '   Show_Message ("unable to Stream: Error:" & FSOUND_GetErrorString(FSOUND_GetError))
    ' Stop_Player
    'Exit Sub
    'Else
    'Call StartBuffering
    'End If
   ' Stream_SetVolume Channel, Volume
    Call StartBuffering(Channel)
    Call FSOUND_Stream_Net_SetMetadataCallback(StreamHandle(Channel), AddressOf FSOUND_METADATACALLBACK, 0)

    Exit Function


On Error Resume Next

' strCurrentUrl = strUrl


Exit Function
HTTPstream_Open_Error:

MsgBox "Error " & err.Number & " (" & err.Description & ") in procedure HTTPstream_Oen of Module mPlayer"

End Function


'---------------------------------------------------------------------------------------
' Procedure : StartBuffering
' Author    : Mahesh Kurmi
' Date      : 3/15/2011
' Purpose   :
' HOW       :
' Returns   :
' Parameters:
'---------------------------------------------------------------------------------------
'
Public Function StartBuffering(lCurrentChannel As Long) As Boolean

    Dim Channel As Long, openState As Integer, status As Long, read_percent As Long, Flags As Long, bitrate As Long
    Channel = -1
    Dim Stream As Long
    Stream = StreamHandle(lCurrentChannel)
    On Error GoTo StartBuffering_Error
    iMetaNum = 0
Do
        If (tLoadedTrack <> HTTP) Then Exit Do
        If (Channel < 0) Then
            Channel = FSOUND_Stream_PlayEx(-1, Stream, 0, True)
            'Channel = ChannelHandle(lCurrentchannel)
            ChannelHandle(lCurrentChannel) = Channel
            FSOUND_SetPaused Channel, False
            If (Channel <> -1) Then
              GetICYtag (Stream): Call FSOUND_Stream_Net_SetMetadataCallback(Stream, AddressOf FSOUND_METADATACALLBACK, 0)
              Exit Function
            End If
            DoEvents
        End If

        '0 = stream is opened and ready.
        '-1 = stream handle passed in is invalid.
        '-2 = stream is still opening or performing a SetSubStream command.
        '-3 = stream failed to open. (file not found, out of memory or other error).
        '-4 = connecting to remote host (internet streams only)
        '-5 = stream is buffering data (internet streams only)
        If openState <> FSOUND_Stream_GetOpenState(Stream) Then
            openState = FSOUND_Stream_GetOpenState(Stream)
            If ((openState = -1) Or (openState = -3)) Then
                frmMain.Show_Message ("failed to open stream!"), True
                frmMain.Timer_Player.Enabled = False
                frmMain.PlayerIsPlaying = "False"
                Exit Do
            ElseIf openState = -5 Then
                frmMain.Timer_Player.Enabled = True
                frmMain.PlayerIsPlaying = "True"
                Stream_SetVolume Channel, frmMain.VolumeNActuaL
            ElseIf openState = -4 Or openState = -2 Then
                frmMain.Show_Message ("Connecting..."), True
            ElseIf openState = 0 Then
                'Exit Do
            End If
        End If
        
        Call FSOUND_Stream_Net_GetStatus(Stream, status, read_percent, bitrate, Flags)
        '   Show how much of the net buffer is used and what the status is
        '  if (metanum)
        ' {
        '   printf("%s - %s\n", artist, title);
        '  metanum = 0;
        ' }
        ' s[0] = 0;
        ' strncat(s, bar, (read_percent >> 1) + (read_percent & 1));
        ' strncat(s, nobar, (100 - read_percent) >> 1);
        ' printf("|%s| %d%%  %s\r", s, read_percent, status_str[status]);

        ' Sleep(16);
        If (iMetaNum) Then
             'Debug.Print tCurrent_HttpSongInfo.Artist & tCurrent_HttpSongInfo.Title
           '  iMetaNum = 0
        End If
   
        '} while (key != 27);
        DoEvents
        DoEvents
        Sleep (16)
    Loop
    'While (tLoadedTrack = HTTP)

    Exit Function

StartBuffering_Error:
    MsgBox "Error " & err.Number & " (" & err.Description & ") in procedure StartBuffering of Form frmMain"
End Function

Private Function pvPtrToStrW(ByVal lpsz As Long) As String
' supporting routine for SaveToPNG
    Dim sOut As String
    Dim lLen As Long

   ' lLen = lstrlenW(lpsz)

    'If (lLen > 0) Then
     '   sOut = StrConv(String$(lLen, vbNullChar), vbProperCase)
     '   Call CopyMemory(ByVal sOut, ByVal lpsz, lLen)
     '   pvPtrToStrW = StrConv(sOut, vbFromUnicode)
    'End If
End Function



Public Function StrFromPtr(ByVal lpStr As Long) As String
            Dim NullCharPos As Long
            Dim pErrMsg
            Dim szBuffer As String
            szBuffer = String(512, 0)
            ' I have tried increasing size of buffer even then it crashes sometimes
            Dim lpString
            On Error GoTo ErrHan:
            If ConvCStringToVBString(szBuffer, lpString) <> vbNull Then
                  ' Look for the null char ending the C string
                  NullCharPos = InStr(szBuffer, vbNullChar)

                 ' If NullCharPos > 0 Then
                        StrFromPtr = Left$(szBuffer, NullCharPos - 1) ' ... using Left$ with a dollar symbol.

                  'Else

                        ' ... too strange to contemplate.

                       ' err.Raise vbObjectError + 1000, , "Someone's playing with my mind man."

                 ' End If

            Else

                  ' ... copy didn't work and nothing to play with.

                  'err.Raise vbObjectError + 1000, , "Copy API Failed, poo to lstrcpyA not working this time."

            End If

ErrRes:

            szBuffer = vbNullString

      Exit Function

ErrHan:

            pErrMsg = err.Description

            Debug.Print "GetStringFromPointer.Error: " & pErrMsg

            Resume ErrRes:
End Function


Public Function GetStringFromPointerold(ByVal lpString As Long) As String
    Dim NullCharPos As Long
    Dim szBuffer As String
    On Error GoTo GetStringFromPointer_Error

    szBuffer = String$(255, Chr(0))
    ConvCStringToVBString szBuffer, lpString
    'Look for the null char ending the C string
    NullCharPos = InStr(szBuffer, vbNullChar)
    
   If NullCharPos > 1 Then
      GetStringFromPointerold = Left(szBuffer, NullCharPos - 1)
   Else
   GetStringFromPointerold = "XXXXXX"
   End If
   ' GetStringFromPointer = Left(szBuffer, NullCharPos - 1)

    Exit Function
GetStringFromPointer_Error:
    MsgBox "Error " & err.Number & " (" & err.Description & ") in procedure GetStringFromPointer of Module mFmod"
    GetStringFromPointerold = ""
End Function

'Example: MyDriverName = GetStringFromPointer(namepointer)
Public Function GetStringFromPointer(ByVal lpString As Long) As String
Dim NullCharPos As Long
Dim szBuffer As String

    szBuffer = String(255, 0)
    ConvCStringToVBString szBuffer, lpString
    ' Look for the null char ending the C string
    NullCharPos = InStr(szBuffer, vbNullChar)
    GetStringFromPointer = Left(szBuffer, NullCharPos - 1)
End Function
Public Function PointerToString(lngPtr As Long) As String
'--------------------------------------------------------
'RETURNS A STRING FROM IT'S POINTER

   Dim strTemp As String
   Dim lngLen As Long
   Dim NullCharPos As Long
   
   
   If lngPtr Then
      lngLen = lstrlenA(lngPtr) * 2
      If lngLen Then
        ' strTemp = Space(lngLen)
         strTemp = String$(lngLen, vbNullChar)
         CopyMemory ByVal strTemp, ByVal lngPtr, lngLen
         PointerToString = strTemp 'Replace(strTemp, Chr(0), "")
      End If
   End If
   
End Function

'\\ --[StringFromPointer]-------------------------
'\\ Returns a VB string from an API returned string pointer
'\\ Parameters:
'\\ lpString - The long pointer to the string
'\\ lMaxlength - the size of empty buffer to allow
'\\ HISTORY:
'\\ DEJ 28/02/2001 Check pointer is a valid string pointer...
'\\ ----------------------------------------------
'\\ You have a royalty free right to use, reproduce, modify, publish and mess with this code
'\\ I'd like you to visit http://www.merrioncomputing.com for updates, but won't force you
'\\ ----------------------------------------------
Public Function StringFromPointer(lpString As Long, lMaxLength As Long) As String

    Dim sRet As String
    Dim lRet As Long

   'StrFromPtr = SysAllocStringByteLen(lpStr, lstrlen(lpStr))

End Function


Public Function B(p1 As Long, p2 As Long) As Boolean
    p1 = p2
    B = CBool(p1)
End Function

Public Function VBStrFromAnsiPtr(ByVal lpStr As Long) As String
Dim bStr() As Byte
Dim cChars As Long
On Error Resume Next
' Get the number of characters in the buffer
cChars = lstrlen(lpStr)
If cChars Then
    ' Resize the byte array
    ReDim bStr(0 To cChars - 1) As Byte
    ' Grab the ANSI buffer
    Call CopyMemory(bStr(0), ByVal lpStr, cChars)
End If
' Now convert to a VB Unicode string
VBStrFromAnsiPtr = StrConv(bStr, vbUnicode)
End Function

